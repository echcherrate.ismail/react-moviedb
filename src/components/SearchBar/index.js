import React, { useState, useEffect, useRef } from "react";
import PropTypes from 'prop-types';

//Image
import searchIcon from '../../images/search-icon.svg';
//styles
import { Wrapper, Content } from "./SearchBar.styles";

//We use {} to set an explicit return, we want to have logic inside!
const SearchBar = ({ setSearchTerm }) => {

    const [state, setState] = useState('');

    const initial = useRef(true);
    //Always triggers when the component mount
    useEffect(() => {
        //Trigger only when user type something
        if (initial.current) {
            initial.current = false;
            return; //Saying -> don't trigger logic below only when it's false.
        }
        const timer = setTimeout(() => {
            setSearchTerm(state);
        }, 500);
        //To void timeout increase.
        return () => clearTimeout(timer);

    }, [setSearchTerm, state])
    return (
        <Wrapper>
            <Content>
                <img src={searchIcon} alt="search-icon" />
                <input
                    type='text'
                    placeholder='Search Movie'
                    onChange={event => setState(event.currentTarget.value)}
                    value={state} />
            </Content>
        </Wrapper>
    )
}

SearchBar.prototypes = {
    callback: PropTypes.func,


}

export default SearchBar;