import { useState, useEffect } from "react";
//API
import API from '../API';
//helpers
import { isPersistedState } from "../helpers";

const initialState = {
    page: 0,
    results: [],
    total_pages: 0,
    total_results: 0
};

export const useHomeFetch = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [state, setState] = useState(initialState);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [isLoadingMore, setIsLoadingMore] = useState(false)


    const fetchMovies = async (page, searchTerm = "") => {
        try {
            setError(false);
            setLoading(true);//for loading spinner
            const movies = await API.fetchMovies(searchTerm, page);
            //Since i have date, i can store their state
            //Important never mutate the state
            setState(prev => ({ ...movies, results: page > 1 ? [...prev.results, ...movies.results] : [...movies.results] }));
        } catch (error) {
            setError(true);
        }

        setLoading(false);
    };


    //Initial render & search
    useEffect(() => {
        //If we search don't check the sessionStorage else Yes.
        if (!searchTerm) {
            const sessionState = isPersistedState('homeState');
            if (sessionState) {
                console.log("Getting data from sessionStorage");
                setState(sessionState);
                return;
            }
        }
        setState(initialState);
        console.log("Getting data from TMDB Api.");
        fetchMovies(1, searchTerm);
    }, [searchTerm]);

    //Load More
    useEffect(() => {
        if (!isLoadingMore) return;
        fetchMovies(state.page + 1, searchTerm);
        setIsLoadingMore(false);
    }, [isLoadingMore, searchTerm, state.page])


    //Write to sessionStorage

    useEffect(() => {
        if (!searchTerm) sessionStorage.setItem('homeState', JSON.stringify(state));
    }, [searchTerm, state]);
    return { state, loading, error, searchTerm, setSearchTerm, setIsLoadingMore };
};